import json
import os
from urllib.parse import urlencode
import requests

from flask import Flask, url_for, make_response, jsonify, request, abort, redirect
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from authlib.integrations.flask_client import OAuth
from authlib.integrations.base_client.base_app import BaseApp
from loginpass import create_flask_blueprint
from loginpass import VK
from datetime import timedelta

from project.config import MainConfig
from project.modules.oauth import handle_authorize
from project.modules.user import user_api_v1, verify_param
from project.modules.utils import MongoJsonEncoder


app = Flask(__name__, subdomain_matching=True)
app.config.from_pyfile('oauth.py')
app.secret_key = "h8w9gh-8gheghfsdfhlgku314hgbfq5atws5xqwaEWUL[GPJHK;HGJctw6yed"
#app.url_map.strict_slashes = False
app.config['SERVER_NAME'] = os.environ.get("SERVER_NAME", "auth.rbxtree.com")
app.config['SERVER_NAME_PROTO'] = os.environ.get("SERVER_NAME_PROTO", "https")
app.config["MONGO_URI"] = os.environ.get('MONGODB_URI')
app.config["MONGO_DB_NAME"] = os.environ.get('MONGO_DB_NAME', "auth")
app.config['DEBUG'] = bool(os.environ.get("DEBUG", "False"))

# 4 vk
app.config['TRUE_REDIRECT_URL'] = os.environ.get("TRUE_REDIRECT_URL", "dtgb.solutions")
app.config['CALLBACK_URL'] = os.environ.get("CALLBACK_URL", "http://localhost:5001/callback")
#"https://dtgb.solutions/front"
app.config['FRONT_VERIFY_REDIRECT_URL'] = os.environ.get('FRONT_REDIR_URL', "http://localhost:5001/front")

CORS(app, resources={r"/*": {"origins": "*"}})

app.json_encoder = MongoJsonEncoder
app.register_blueprint(user_api_v1)

jwt = JWTManager(app)
oauth = OAuth(app)

app.config['JWT'] = jwt
app.config['BCRYPT'] = Bcrypt(app)
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(days=1)


def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@app.route("/site-map", endpoint="site_map")
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))
        if "POST" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))
    return make_response(jsonify(links)), 200


#: you can customize this part
backends = [VK]


@app.route('/oauth')
def index():
    vk_url = str(app.config.get('VK_AUTH_URL'))\
            .replace('CLIENT_ID', app.config.get('VK_CLIENT_ID'))\
            .replace('CALLBACK', app.config.get('CALLBACK_URL'))
    tpl = '<li><a href="{}">vk</a></li>'.format(vk_url)
    return '<ul>' + tpl + '</ul>'


@app.route('/front')
def front():
    q = request.args.get('q')
    """ 4 front
    req_url = app.config['SERVER_NAME_PROTO'] + "://" + app.config['SERVER_NAME'] + "/api/v1/user/verify?" + urlencode({'q': q})
    headers = {'Content-Type': 'application/json'}
    print(req_url)
    """
    try:
        r = verify_param(q)
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500
    #user_info = json.loads()
    tpl = '<p>VK auth complete.</p>'
    tpl += "<br/>"
    tpl += "<pre>"
    tpl += str(r)
    tpl += "</pre>"
    return tpl


@app.route('/callback')
def callback():
    try:
        if request.args.get('error'):
            redirect_url = app.config['FRONT_VERIFY_REDIRECT_URL'] + "?q=0"
            return redirect(redirect_url, code=302)
        code = request.args.get('code')
        access_url = app.config.get('VK_ACCESS_URL')\
            .replace('CLIENT_ID', app.config.get('VK_CLIENT_ID'))\
            .replace('CLIENT_SECRET', app.config.get('VK_CLIENT_SECRET'))\
            .replace('CALLBACK', app.config.get('CALLBACK_URL'))\
            .replace('CODE', code)
        headers = {'Content-Type': 'application/json'}
        r = requests.get(access_url, headers=headers)
        print(r.text)
        access_token = json.loads(r.text)
        user_get_url = app.config.get('VK_USER_GET')\
            .replace('USER_ID', str(access_token['user_id']))\
            .replace('ACCESS_TOKEN', str(access_token['access_token']))
        headers = {'Content-Type': 'application/json'}
        r = requests.get(user_get_url, headers=headers)
        print(r.text)
        user_info = json.loads(r.text)
        redirect_url = handle_authorize(remote='', token='', user_info=user_info['response'][0])
        return redirect(redirect_url, code=302)
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
    return make_response(jsonify(response_object)), 500


bp = create_flask_blueprint(backends, oauth, handle_authorize)
app.register_blueprint(bp, url_prefix='')

CORS(bp, resources={r"/*": {"origins": "*"}})


#app.config.from_object(os.environ['APP_SETTINGS'])
PresentConfig = MainConfig
app.config.from_object(PresentConfig)
#app = create_app()


if __name__ == '__main__':
    app.run()
