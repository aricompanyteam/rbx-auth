import os


class MainConfig(object):
    """Production configuration."""
    ADMIN_SECRET_KEY = '983esn4u3segkdfs7845adfshg4n45879tsgerudfg'
    SECRET_KEY = '4wgq3wrg3wdfsh4faw34w'
    BCRYPT_LOG_ROUNDS = 13
    WTF_CSRF_ENABLED = True
    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    STRIPE_SECRET_KEY = 'foo'
    STRIPE_PUBLISHABLE_KEY = 'bar'
    MAIL_SENDER = "noreply@rbxtree.com"
    MAIL_SERVER = 'mail.rbxtree.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = False
    MAIL_USERNAME = os.environ.get('EMAIL_USER', 'mail.rbxtree.com-vmta')
    MAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD', '34Mald4323afg')
