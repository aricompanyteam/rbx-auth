# these need to be called before the app gets init
from werkzeug.middleware.proxy_fix import ProxyFix
from project.modules.factory import create_app

app = create_app()
app.wsgi_app = ProxyFix(app.wsgi_app)

# now we can init the app
if __name__ == "__main__":
    app.run()
