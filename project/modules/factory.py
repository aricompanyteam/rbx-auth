import os

from flask import Flask
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from datetime import timedelta
from project.modules.user import user_api_v1
from project.modules.utils import MongoJsonEncoder


def create_app():
  app = Flask(__name__, subdomain_matching=True)
  app.secret_key = "h8w9gh-8gheghfsdfhlgku314hgbfq5atws5xqwaEWUL[GPJHK;HGJctw6yed"

  app.config['SERVER_NAME'] = os.environ.get("SERVER_NAME", "rbxtree.com")
  app.config["MONGO_URI"] = os.environ.get('MONGO_URI')
  app.config["MONGO_DB_NAME"] = os.environ.get('MONGO_DB_NAME', "auth")
  app.config.from_object(os.environ['APP_SETTINGS'])

  CORS(app, resources={r"/*": {"origins": "*"}})
  app.json_encoder = MongoJsonEncoder
  app.register_blueprint(user_api_v1)
  jwt = JWTManager(app)

  app.config['JWT'] = jwt
  app.config['BCRYPT'] = Bcrypt(app)
  app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(days=1)
  app.config['DEBUG'] = bool(os.environ.get("DEBUG", "False"))

  return app
