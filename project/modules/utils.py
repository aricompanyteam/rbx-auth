import os
from datetime import datetime
from hashlib import sha256
from hmac import new

from bson import json_util, ObjectId
from flask.json import JSONEncoder
from time import mktime
from string import digits
from random import choice
from flask import current_app
from flask_mail import Message, Mail


# TODO send email with reset url
def send_reset_link(link, email):
  mail = Mail(current_app)
  text = "Здравствуйте!\nМы получили запрос на восстановление доступа к Вашей учетной записи.\n" \
         "Чтобы изменить пароль, используйте код восстановления : " + link
  msg = Message(subject="Восстановление доступа к Вашей учетной записи",
                sender=current_app.config.get("MAIL_SENDER"),
                recipients=[email],
                body=text)
  print(current_app.config.get("MAIL_SENDER"), email, text)
  mail.send(msg)


def expect(input, expectedType, field):
  if isinstance(input, expectedType):
    return input
  raise AssertionError("Invalid input for type", field)


def get_diff_minutes(verify):
  fmt = '%Y-%m-%d %H:%M:%S'
  d1 = datetime.strptime(verify['date_request'], fmt)
  d2 = datetime.now()
  # Convert to Unix timestamp
  d1_ts = mktime(d1.timetuple())
  d2_ts = mktime(d2.timetuple())
  diff_minutes = int(d2_ts - d1_ts) / 60

  return diff_minutes


def get_diff_minutes_sign(verify):
  d1 = datetime.fromisoformat(verify)
  d2 = datetime.now()
  # Convert to Unix timestamp
  d1_ts = mktime(d1.timetuple())
  d2_ts = mktime(d2.timetuple())
  diff_minutes = int(d2_ts - d1_ts) / 60

  return diff_minutes


def get_signature(string):
  key = os.environ.get("access_key", "sD23HAdhgASDnsdhaDW76wdgdxcdsgUsdSDGikd")
  return new(bytes(key, 'utf-8'), bytes(string, 'utf-8'), sha256).hexdigest()


def id_generator(size=6, chars=digits):
  return ''.join(choice(chars) for _ in range(size))


class MongoJsonEncoder(JSONEncoder):
  def default(self, obj):
    if isinstance(obj, datetime):
      return obj.strftime("%Y-%m-%d %H:%M:%S")
    if isinstance(obj, ObjectId):
      return str(obj)
    return json_util.default(obj, json_util.CANONICAL_JSON_OPTIONS)
