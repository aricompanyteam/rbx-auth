from datetime import datetime
from random import randint

import bcrypt
from flask import current_app, g, redirect
from werkzeug.local import LocalProxy
from pymongo import MongoClient, DESCENDING, ASCENDING
from pymongo.errors import DuplicateKeyError, OperationFailure
from bson.objectid import ObjectId
from project.modules.utils import id_generator


def handle_authorize_db(remote, token, user_info):
    try:
        surname = user_info['family_name']
        username = user_info['given_name']
        surcap = surname[0]

        if surcap:
            username = username + " " + surcap + "."
        else:
            username = username
        password = str(randint(1, 1000))
        try:
            picture = user_info['picture']
        except:
            picture = randint(1, 10)
        userdata = {
            "username": username,
            "username_lowercase": str(username).lower(),
            "email": "",
            "ref_code": "",
            "is_vk": 1,
            "avatar": picture,
            "user_id": randint(1, 1000),
            "sub": user_info['sub'],
            'verify_code': id_generator()
        }

        add_user(userdata, bcrypt.generate_password_hash(
            password=password.encode('utf8')).decode("utf-8"))
        redurect_url = current_app.config['FRONT_VERIFY_REDIRECT_URL'] + "?q=" + userdata['verify_code']
        return redirect(redurect_url, code=302)
    except Exception as ex:
        print(ex)
        redurect_url = current_app.config['FRONT_VERIFY_REDIRECT_URL'] + "?q=0"
        return redirect(redurect_url, code=302)


def get_db():
    """
    Configuration method to return db instance
    """
    db = getattr(g, "_database", None)
    APP_DB_URI = current_app.config["MONGO_URI"]
    APP_DB_NAME = current_app.config["MONGO_DB_NAME"]
    # TODO logging internal info
    if db is None:
        #try:
        conn = MongoClient(
            APP_DB_URI
        )
        db = g._database = conn[APP_DB_NAME]
        #except Exception as ex:
        #    return {"error": "Failed connect to DB"}
        #else:
        #    print("Successfully connected to DB")
    return db


# Use LocalProxy to read the global db instance with just `db`
db = LocalProxy(get_db)


"""
- get_user
- add_user
- login_user
- logout_user
- get_user_session
- delete_user
"""


def get_settings(id):
    obj = {}
    try:
        obj = db.settings.find_one({"set_id": id})
        if obj is None:
            insert_settings()
    except:
        obj = None
    if obj is None:
        obj = db.settings.find_one({"set_id": id})
    return obj


def insert_settings():
    """
    Insert settings
    """
    try:
        db.settings.insert_one({"set_id": 1, "time_limit": 3600})
        return {"success": True}
    except Exception as e:
        return {"error": e}


def get_user(email):
    """
    Given an email, returns a document from the `users` collection.
    """
    obj = {}
    try:
        obj = db.users.find_one({"email": str(email).lower()})
        if obj is None:
            obj = db.users.find_one({"username_lowercase": str(email).lower()})
        if obj is None:
            obj = db.users.find_one({"_id": ObjectId(email)})
    except:
        obj = None
    return obj


def get_user_verify(q):
    """
    Given an query, returns a document from the `users` collection.
    """
    obj = {}
    try:
        obj = db.users.find_one({"verify_code": q})
    except:
        obj = None
    return obj


def update_user(id, hashpw):
    """
    Update user
    """
    try:
        db.users.update_one({"_id": ObjectId(id)},
                                   {"$set": {"password": hashpw}})
        return {"success": True}
    except Exception as e:
        return {"error": e}


def update_balance(id, amount):
    """
    Balance user
    """
    try:
        db.users.update_one({"_id": ObjectId(id)},
                                   {"$inc": {"rub_balance": amount}})
        return {"success": True}
    except Exception as e:
        return {"error": e}


def generate_reset_url(userdata):
  """
  Generate reset url
  """
  hashurl = ""
  try:
      # old
      # hashurl = hmac.new(bytes(current_app.secret_key, 'latin-1'),
      #                    msg=bytes(str(userdata['user_id']) + " " + str(random.randint(1, 1000))
      #                              + " " + str(userdata['username']), 'latin-1'),
      #                    digestmod=hashlib.sha256).hexdigest().upper()
      # new
      hashurl = id_generator()
      db.update_password.update_one({"user_id": userdata['user_id']},
                          {"$set": {
                              "verify_token": hashurl,
                              "status": "new",
                              "date_request": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                            }
                          }, upsert=True)
      return hashurl
  except Exception as e:
      return {"error": e}


def update_update_password(user_id):
    try:
        db.update_password.update_one({"user_id": user_id},
                                      {"$set": {"status": "expared"}})
    except Exception as e:
      return {"error": e}


def get_token(token):
    obj = {}
    try:
        obj = db.update_password.find_one({"verify_token": token})
    except:
        obj = None
    return obj


def add_user(data, hashedpw):
    """
    Given a username, email, ref_code and password, inserts a document with those credentials
    to the `users` collection.
    """
    try:
        # TODO Use a more durable Write Concern for this operation.
        if db.users.find({'email': data['email']}).count() > 0 and data['is_vk'] == 0:
            return {"error": "A user with the given email already exists."}
        elif db.users.find({'username_lowercase': data['username_lowercase']}).count() > 0 and data['is_vk'] == 0:
            return {"error": "A user with the given username already exists."}
        elif data['is_vk'] == 1 and db.users.find({'sub': data['sub']}).count() > 0:
            db.users.update_one(
                {"sub": data['sub']},
                {"$set": {
                    "verify_code": data['verify_code'],
                }})
        else:
            db.users.insert_one({
                    "username": data['username'],
                    "username_lowercase": data['username_lowercase'],
                    "email": data['email'],
                    "ref_code": data['ref_code'],
                    "rub_balance": 0.00,
                    "is_vk": data['is_vk'],
                    "avatar": data['avatar'],
                    "sub": data['sub'],
                    "verify_code": str(data['verify_code']),
                    "password": hashedpw
                })
            return {"success": True}
    except DuplicateKeyError:
        return {"error": "A user with the given email already exists."}


def login_user(email, jwt):
    """
    Given an email and JWT, logs in a user by updating the JWT corresponding
    with that user's email in the `sessions` collection.

    In `sessions`, each user's email is stored in a field called "user_id".
    """
    try:
        # Use an UPSERT statement to update the "jwt" field in the document,
        # matching the "user_id" field with the email passed to this function.
        db.sessions.update_one(
            {"email": str(email).lower()},
            {"$set": {"user_id": jwt}},
            upsert=True
        )
        return {"success": True}
    except Exception as e:
        return {"error": e}


def logout_user(email):
    """
    Given a user's email, logs out that user by deleting their corresponding
    entry in the `sessions` collection.

    In `sessions`, each user's email is stored in a field called "user_id".
    """
    try:
        # Delete the document in the `sessions` collection matching the email.
        db.sessions.delete_one({"email": email})
        return {"success": True}
    except Exception as e:
        return {"error": e}


def get_user_session(email):
    """
    Given a user's email, finds that user's session in `sessions`.

    In `sessions`, each user's email is stored in a field called "user_id".
    """
    try:
        # Retrieve the session document corresponding with the user's email.
        return db.sessions.find_one({"email": str(email).lower()})
    except Exception as e:
        return {"error": e}


def delete_user(email):
    """
    Given a user's email, deletes a user from the `users` collection and deletes
    that user's session from the `sessions` collection.
    """
    try:
        # Delete the corresponding documents from `users` and `sessions`.
        db.sessions.delete_one({"email": email})
        db.users.delete_one({"email": email})
        if get_user(email) is None:
            return {"success": True}
        else:
            raise ValueError("Deletion unsuccessful")
    except Exception as e:
        return {"error": e}


def get_configuration():
    """
    Returns the following information configured for this client:

    - max connection pool size
    - write concern
    - database user role
    """

    try:
        role_info = db.command({'connectionStatus': 1}).get('authInfo').get(
            'authenticatedUserRoles')[0]
        return (db.client.max_pool_size, db.client.write_concern, role_info)
    except IndexError:
        return (db.client.max_pool_size, db.client.write_concern, {})
