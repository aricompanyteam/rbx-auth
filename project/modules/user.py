from random import randint
from flask import make_response, jsonify, request
from flask_cors import CORS

from project.modules.db import get_user, add_user, login_user, logout_user, \
    delete_user, update_user, generate_reset_url, get_token, get_settings, update_update_password, get_user_verify, \
    update_balance
from project.modules.utils import expect, get_diff_minutes, id_generator, get_signature, get_diff_minutes_sign
from bson.json_util import dumps, loads
from email_validator import validate_email, EmailNotValidError
from datetime import datetime
from flask import Blueprint
from flask_jwt_extended import (
    jwt_required, create_access_token,
    create_refresh_token, get_jwt_identity,
)
from flask import current_app, g
from werkzeug.local import LocalProxy
from functools import wraps
from project.modules.utils import send_reset_link


user_api_v1 = Blueprint('user_api_v1', __name__, url_prefix='/api/v1/user')

CORS(user_api_v1, resources={r"/*": {"origins": "*"}})


def get_jwt():
    jwt = getattr(g, '_jwt', None)
    if jwt is None:
        jwt = g._jwt = current_app.config['JWT']

    return jwt


def get_bcrypt():
    bcrypt = getattr(g, '_bcrypt', None)
    if bcrypt is None:
        bcrypt = g._bcrypt = current_app.config['BCRYPT']
    return bcrypt


def init_claims_loader():
    add_claims = getattr(g, '_add_claims', None)
    if add_claims is None:
        add_claims = g._add_claims = current_app.config['ADD_CLAIMS']
    return add_claims


jwt = LocalProxy(get_jwt)
bcrypt = LocalProxy(get_bcrypt)
add_claims_to_access_token = LocalProxy(init_claims_loader)


def jwt_required_gcp(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        id_token = request.headers['Authorization'].split(' ').pop()
        #claims = google.oauth2.id_token.verify_firebase_token(id_token, HTTP_REQUEST)
        claims = {}
        if not claims:
            return 'Unauthorized', 401
        return fn(*args, **kwargs)
    return wrapper

# def my_decorator(f):
#     @wraps(f)
#     def wrapper(*args, **kwds):
#         return f(*args, **kwds)
#     return wrapper


class User(object):
    def __init__(self, userdata):
        self.email = userdata.get('email')
        self.username = userdata.get('name')
        self.password = userdata.get('password')
        self.rub_balance = userdata.get('rub_balance', 0)
        self.ref_code = userdata.get('ref_code', "")
        self.is_vk = userdata.get('is_vk', "0")
        self.avatar = userdata.get('avatar', randint(1, 10))
        self.date_registration = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    def to_json(self):
        return loads(dumps(self, default=lambda o: o.__dict__, sort_keys=True))

    @staticmethod
    def from_claims(claims):
        return User(claims.get('user'))


@user_api_v1.route('/register', methods=['POST'])
def register():
    try:
        post_data = request.get_json()
        email = expect(post_data['email'], str, 'email')
        username = expect(post_data['username'], str, 'username')
        password = expect(post_data['password'], str, 'password')
    except Exception as e:
        return jsonify({'error': str(e)}), 400

    try:
        ref_code = expect(post_data['ref_code'], str, 'ref_code')
    except:
        ref_code = ""

    errors = {}
    if len(password) < 6:
        errors['password'] = "Your password must be at least 6 characters."

    if len(username) < 4:
        errors['username'] = "You must specify a name of at least 4 characters."

    try:
        valid = validate_email(email)
    except EmailNotValidError as e:
        errors['email'] = "email is not valid"

    if len(errors.keys()) != 0:
        response_object = {
            'status': False,
            'error': errors
        }
        return jsonify(response_object), 411

    userdata = {
        "username": username,
        "username_lowercase": str(username).lower(),
        "email": str(email).lower(),
        "ref_code": ref_code,
        "is_vk": 0,
        "avatar": randint(1, 10),
        "sub": 0,
        "verify_code": id_generator()
    }
    insertionresult = add_user(userdata, bcrypt.generate_password_hash(
        password=password.encode('utf8')).decode("utf-8"))
    if 'error' in insertionresult:
        errors['email'] = insertionresult["error"]

    if len(errors.keys()) != 0:
        response_object = {
            'error': errors
        }
        return make_response(jsonify(response_object)), 400
    else:
        userdata = get_user(email)
        if not userdata:
            errors['general'] = "Internal error, please try again later."
        if len(errors.keys()) != 0:
            response_object = {
                'error': errors
            }
            return make_response(jsonify(response_object)), 400

        userdata = {
            "user_id": userdata["_id"],
            "email": userdata['email'],
            "username": userdata['username'],
            "username_lowercase": userdata.get('username_lowercase'),
            "rub_balance": userdata.get('rub_balance'),
            "is_vk": userdata.get('is_vk'),
            "avatar": userdata.get('avatar'),
        }

        user = User(userdata)
        token = {
            "access": create_access_token(identity=user.to_json()),
            "refresh": create_refresh_token(identity=user.to_json())
        }

        try:
            login_user(user.email, token)
            response_object = {
                'auth_token': token,
                'data': userdata
            }
            return make_response(jsonify(response_object)), 201
        except Exception as e:
            response_object = {
                'error': {'internal': e}
            }
            return make_response(jsonify(response_object)), 500


@user_api_v1.route('/login', methods=['POST'])
def login():
    email = ""
    password = ""
    try:
        post_data = request.get_json()
        email = expect(post_data['login'], str, 'login')
        password = expect(post_data['password'], str, 'password')
    except Exception as e:
        jsonify({'error': str(e)}), 400

    userdata = get_user(email)
    if not userdata:
        response_object = {
            'error': {'email': 'Make sure your email/username is correct.'}
        }
        return make_response(jsonify(response_object)), 401
    if not bcrypt.check_password_hash(userdata['password'], password):
        response_object = {
            'error': {'password': 'Make sure your password is correct.'}
        }
        return make_response(jsonify(response_object)), 401

    userdata = {
        "user_id": userdata['_id'],
        "email": userdata['email'],
        "username": userdata['username'],
        "username_lowercase": userdata.get('username_lowercase'),
        "rub_balance": userdata.get('rub_balance'),
        "is_vk": userdata.get('is_vk'),
        "avatar": userdata.get('avatar'),
    }

    user = User(userdata)
    token = {
        'access': create_access_token(user.to_json()),
        'refresh': create_refresh_token(user.to_json())
    }

    try:
        login_user(user.email, token)
        response_object = {
            'auth_token': token,
            'data': userdata,
        }
        return make_response(jsonify(response_object)), 201
    except Exception as e:
        response_object = {
            'error': {'internal': e}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/refresh', methods=['POST'], endpoint="refresh")
@jwt_required(refresh=True)
def refresh():
    claims = get_jwt_identity()
    user = get_user(claims.get('email'))
    try:
        userdata = {
            "user_id": user['_id'],
            "email": user['email'],
            "username": user['username'],
            "username_lowercase": user.get('username_lowercase'),
            "rub_balance": user.get('rub_balance'),
            "is_vk": user.get('is_vk'),
            "avatar": user.get('avatar')
        }
        user = User(userdata)
        token = {
            'access': create_access_token(user.to_json()),
            'refresh': create_refresh_token(user.to_json())
        }
        login_user(user.email, token)
        response_object = {
            'auth_token': token,
            'data': userdata,
        }
        return make_response(jsonify(response_object)), 201
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/logout', methods=['POST'], endpoint="logout")
@jwt_required()
def logout():
    claims = get_jwt_identity()
    user = get_user(claims.get('email'))
    try:
        logout_user(user.get('email'))
        response_object = {
            'status': True
        }
        return make_response(jsonify(response_object)), 201
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 401


@user_api_v1.route('/delete', methods=['DELETE'], endpoint='delete')
@jwt_required
def delete():
    claims = get_jwt_identity()
    user = get_user(claims.get('email'))
    try:
        password = expect(request.get_json().get('password'), str, 'password')
        userdata = get_user(user.email)
        if (not user.email == userdata['email'] and not
                bcrypt.check_password_hash(userdata['password'], password)):
            response_object = {
                'error': {'password': 'Make sure your password is correct.'}
            }
            return make_response(jsonify(response_object)), 401
        else:
            delete_user(user.email)
            response_object = {
                'status': True
            }
            return make_response(jsonify(response_object)), 201
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/profile', methods=['GET'], endpoint='profile')
@jwt_required()
def profile():
    claims = get_jwt_identity()
    user = get_user(claims.get('email'))
    try:
        if user is not None:
            userdata = {
                "user_id": user['_id'],
                "email": user['email'],
                "username": user['username'],
                "username_lowercase": user.get('username_lowercase'),
                "rub_balance": user['rub_balance'],
                "is_vk": user.get('is_vk'),
                "avatar": user.get('avatar')
            }
            response_object = {
                'data': userdata,
                'status': True
            }
            return make_response(jsonify(response_object)), 200
        else:
            response_object = {
                'status': False
            }
            return make_response(jsonify(response_object)), 400
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/info', methods=['GET'], endpoint='info')
def info():
    try:
        q = request.args.get('q')
        user = get_user(q)
        if user is not None:
            userdata = {
                "user_id": user['_id'],
                "email": user['email'],
                "username": user['username'],
                "username_lowercase": user.get('username_lowercase'),
            }
            response_object = {
                'data': userdata,
                'status': True
            }
            return make_response(jsonify(response_object)), 200
        else:
            response_object = {
                'q': q,
                'status': False
            }
            return make_response(jsonify(response_object)), 400
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/update-password', methods=['POST'], endpoint='update_password')
@jwt_required()
def update_password():
    claims = get_jwt_identity()
    user = get_user(claims.get('email'))
    try:
        userdata = get_user(user.get('email'))
        old_password = expect(request.get_json().get('old'), str, 'old')
        new_password = expect(request.get_json().get('new'), str, 'new')
        if not bcrypt.check_password_hash(userdata['password'], old_password):
            response_object = {
                'error': {'password': 'Make sure your password is correct.'}
            }
            return make_response(jsonify(response_object)), 401
        else:
            bstr = bcrypt.generate_password_hash(password=new_password.encode('utf8')).decode("utf-8")
            e = update_user(user['_id'], bstr)

            response_object = {
                'status': True
            }
            return make_response(jsonify(response_object)), 200
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/reset-password', methods=['POST'], endpoint='reset_password')
def reset_password():
    try:
        post_data = request.get_json()
        user = get_user(expect(post_data['login'], str, 'login'))
        if user is not None:
            userdata = {
                "user_id": user['_id'],
                "email": user['email'],
                "username": user['username'],
            }
            hashurl = generate_reset_url(userdata)
            send_reset_link(hashurl, user['email'])

            response_object = {
                'code': hashurl,
                'status': True
            }
            return make_response(jsonify(response_object)), 200
        else:
            response_object = {
                'q': post_data,
                'status': False
            }
            return make_response(jsonify(response_object)), 400
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/verify-token', methods=['POST'], endpoint='verify_token')
def verify_token():
    settings = get_settings(int(1))
    try:
        post_data = request.get_json()
        verify = get_token(expect(post_data['token'], str, 'token'))
        if verify is not None:
            time_limit = settings['time_limit']
            diff_minutes = get_diff_minutes(verify)
            if verify['status'] == 'new' and diff_minutes < time_limit:
                tokendata = {
                    'd': time_limit,
                    'a': diff_minutes,
                    "user_id": verify['user_id'],
                    "verify_token": verify['verify_token']
                }
                response_object = {
                    'data': tokendata,
                    'status': True
                }
                return make_response(jsonify(response_object)), 200
            else:
                response_object = {
                    'd': time_limit,
                    'a': diff_minutes,
                    'msg': "Token has been expired",
                    'status': False
                }
                return make_response(jsonify(response_object)), 401
        else:
            response_object = {
                'q': post_data,
                'status': False
            }
            return make_response(jsonify(response_object)), 400
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/update-password-token', methods=['POST'], endpoint='update_password_token')
def update_password_token():
    settings = get_settings(int(1))
    try:
        post_data = request.get_json()
        verify = get_token(expect(post_data['token'], str, 'token'))
        if verify is not None:
            userdata = get_user(verify.get('user_id'))
            new_password = expect(request.get_json().get('new'), str, 'new')
            bstr = bcrypt.generate_password_hash(password=new_password.encode('utf8')).decode("utf-8")
            time_limit = settings['time_limit']
            diff_minutes = get_diff_minutes(verify)
            if verify['status'] == 'new' and diff_minutes < time_limit:
                update_user(userdata['_id'], bstr)
                update_update_password(userdata['_id'])

                response_object = {
                    'status': True
                }
                return make_response(jsonify(response_object)), 200
            else:
                response_object = {
                    'msg': "Token has been expired",
                    'status': False
                }
                return make_response(jsonify(response_object)), 401
        else:
            response_object = {
                'q': post_data,
                'status': False
            }
            return make_response(jsonify(response_object)), 400
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
        return make_response(jsonify(response_object)), 500


@user_api_v1.route('/verify', methods=['GET'], endpoint='verify')
@user_api_v1.route('/user_verify', methods=['GET'], endpoint='verify')
def verify():
    try:
        q = request.args.get('q')
        user = get_user_verify(q)
        if user is not None:
            userdata = {
                "user_id": user['_id'],
                "email": user['email'],
                "username": user['username'],
                "username_lowercase": user.get('username_lowercase'),
                "rub_balance": user['rub_balance'],
                "is_vk": user.get('is_vk'),
                "avatar": user.get('avatar')
            }
            user = User(userdata)
            token = {
                'access': create_access_token(user.to_json()),
                'refresh': create_refresh_token(user.to_json())
            }
            login_user(user.email, token)
            response_object = {
                'auth_token': token,
                'data': userdata,
            }
            return make_response(jsonify(response_object)), 201
        else:
            response_object = {
                'q': q,
                'status': False
            }
            return make_response(jsonify(response_object)), 400
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
    return make_response(jsonify(response_object)), 500


def verify_param(q):
    try:
        user = get_user_verify(q)
        if user is not None:
            userdata = {
                "user_id": user['_id'],
                "email": user['email'],
                "username": user['username'],
                "username_lowercase": user.get('username_lowercase'),
                "rub_balance": user['rub_balance'],
                "is_vk": user.get('is_vk'),
                "avatar": user.get('avatar')
            }
            user = User(userdata)
            token = {
                'access': create_access_token(user.to_json()),
                'refresh': create_refresh_token(user.to_json())
            }
            login_user(user.email, token)
            response_object = {
                'auth_token': token,
                'data': userdata,
            }
            return response_object
        else:
            response_object = {
                'q': q,
                'status': False
            }
            return response_object
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
    return response_object


@user_api_v1.route('/vkurl', methods=['GET'], endpoint='vkurl')
def vk_url():
    try:
        callback = str(current_app.config.get('VK_AUTH_URL'))\
            .replace('CLIENT_ID', current_app.config.get('VK_CLIENT_ID'))\
            .replace('CALLBACK', current_app.config.get('CALLBACK_URL'))
        response_object = {
            'vk_url': callback,
        }
        return make_response(jsonify(response_object)), 200
    except Exception as e:
        response_object = {
            'error': {'internal': str(e)}
        }
    return make_response(jsonify(response_object)), 500


@user_api_v1.route('/balance', methods=['PUT'], endpoint='inc_balance')
def inc_balance():
    try:
        post_data = request.get_json()
        print(post_data)
        user_id = expect(post_data['user_id'], str, "user_id")
        amount = expect(post_data['amount'], float, "amount")
        date = post_data['date']
        post_sign = expect(post_data['signature'], str, "signature")
        user = get_user(user_id)
        if user:
            delta_balance = user.get('rub_balance') + amount
            print(delta_balance)
            s_dict = {"user_id": user_id, "amount": amount, "date": date}
            formatted_string = ';'.join([f'{key}:{s_dict[key]}' for key in sorted(s_dict.keys())]) + ';'
            signature = get_signature(formatted_string)
            print(signature)
            delta = get_diff_minutes_sign(date)
            if 0 <= delta <= 5 and signature == post_sign and delta_balance >= 0:
                update_balance(user_id, amount)
                response_object = {
                    'status': True,
                }
                return make_response(jsonify(response_object)), 200
            else:
                response_object = {
                    'd': delta,
                    'status': False,
                }
                return make_response(jsonify(response_object)), 401
        else:
            response_object = {
                'status': False,
            }
            return make_response(jsonify(response_object)), 400
    except Exception as e:
        print(e)
        response_object = {
            'status': False,
            'error': {'internal': str(e)}
        }
    return make_response(jsonify(response_object)), 500
