"""
 oauth handle
"""
import bcrypt
from random import randint
from flask import current_app, redirect

from project.modules.db import id_generator, add_user


def handle_authorize(remote, token, user_info):
  print(user_info)
  try:
    surname = user_info['last_name']
    username = user_info['first_name']
    surcap = surname[0]

    if surcap:
      username = username + " " + surcap + "."
    else:
      username = username
    password = str(randint(1, 1000))
    sub = user_info['id']
    try:
      picture = user_info['photo_max']
    except:
      picture = randint(1, 10)
    userdata = {
      "username": username,
      "username_lowercase": str(username).lower(),
      "email": "",
      "ref_code": "",
      "is_vk": 1,
      "avatar": picture,
      "user_id": randint(1, 1000),
      "sub": sub,
      'verify_code': id_generator()
    }

    add_user(userdata, '')
    redurect_url = current_app.config['FRONT_VERIFY_REDIRECT_URL'] + "?q=" + userdata['verify_code']
    return redurect_url
  except Exception as ex:
    print(ex)
    redurect_url = current_app.config['FRONT_VERIFY_REDIRECT_URL'] + "?q=0"
    return redurect_url
