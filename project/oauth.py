VK_AUTH_URL = "https://oauth.vk.com/authorize?client_id=CLIENT_ID&display=page&redirect_uri=CALLBACK&scope=email&response_type=code&v=5.130"
VK_ACCESS_URL = "https://oauth.vk.com/access_token?client_id=CLIENT_ID&client_secret=CLIENT_SECRET&redirect_uri=CALLBACK&code=CODE"
VK_USER_GET = "https://api.vk.com/method/users.get?user_id=USER_ID&v=5.52&access_token=ACCESS_TOKEN&fields=photo_max"
""" dev"""
#VK_CLIENT_ID = 'XXX'
#VK_CLIENT_SECRET = 'XXX'
""" prod """
VK_CLIENT_ID = 'XXX'
VK_CLIENT_SECRET = 'XXX'
